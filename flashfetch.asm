section .text
global _start

_start:
	; Open /etc/os-release
	mov rax, 2
	mov rdi, OS_RELEASE_PATH
	xor rsi,rsi
	syscall
	
	mov [OS_RELEASE_FD], rax
	
	; Get file info
	mov rax, 5
	mov rdi, [OS_RELEASE_FD]
	mov rsi, OS_RELEASE_STAT_BUFF
	syscall

	; Store size of file in rbx
	mov rbx, [OS_RELEASE_STAT_BUFF+48]
	; Increase data segment to handle different file sizes
	mov rax, 12
	mov rdi, [mem+rbx]
	syscall

	; Read the hole file
	mov rdx, [OS_RELEASE_STAT_BUFF+48]
	mov rax, 0
	mov rdi, [OS_RELEASE_FD]
	mov rsi, mem
	syscall

	mov rcx, 0
get_id_loop:
	mov ax, [mem+rcx]
	cmp ax, 'ID'
	jz found_id
newline_loop:
	mov al, [mem+rcx]
	inc rcx
	cmp al, 0x0a 
	jz get_id_loop
	jmp newline_loop
found_id:
	add rcx, 3
	mov rdx, 0
	mov r10, rcx
find_newline:
	mov al, [mem+r10]
	cmp al, 0x0a
	jz id_write
	inc rdx
	inc r10
	jmp find_newline
id_write:
	mov rbx, 0
id_write_loop:
	mov al, [mem+rcx]
	mov [mem+rbx], al
	cmp rbx, rdx
	je finish_distro_get
	inc rbx
	inc rcx
	jmp id_write_loop
finish_distro_get:
	mov rbx, rdx
	mov rax, 12
	add rdx, mem
	mov rdi, rdx
	syscall

	; DEBUG: print the file
	mov rax, 1
	mov rdi, 1
	mov rsi, mem
	mov rdx, rbx 
	syscall
loop:
	jmp loop

section .data
OS_RELEASE_PATH: db '/etc/os-release',0
section .bss
OS_RELEASE_STAT_BUFF: resb 144
OS_RELEASE_FD: resb 4
mem:
